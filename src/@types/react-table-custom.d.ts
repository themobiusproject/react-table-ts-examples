import { HTMLAttributes } from "react";
import {
  TableProps,
  TableBodyProps,
  TableHeaderGroupProps,
  TableFooterGroupProps,
  TableHeaderProps,
  TableFooterProps,
  TableRowProps,
  TableCellProps,
} from "react-table";

declare module "react-table" {
  export interface TableAttr {
    tableProps?: Partial<TableProps>;
    bodyProps?: Partial<TableBodyProps>;
    headerGroupProps?: Partial<TableHeaderGroupProps>;
    footerGroupProps?: Partial<TableFooterGroupProps>;
    rowProps?: Partial<TableRowProps>;
  }

  export interface TableColumnAttr {
    headerProps?: Partial<TableHeaderProps>;
    cellProps?: Partial<TableCellProps>;
    footerProps?: Partial<TableFooterProps>;
  }

  export interface TableAttrExt {
    tableProps?: HTMLAttributes<HTMLTableElement> & Partial<TableProps>;
    bodyProps?: HTMLAttributes<HTMLTableSectionElement> & Partial<TableBodyProps>;
    headerGroupProps?: HTMLAttributes<HTMLTableRowElement> & Partial<TableHeaderGroupProps>;
    footerGroupProps?: HTMLAttributes<HTMLTableSectionElement> & Partial<TableFooterGroupProps>;
    rowProps?: HTMLAttributes<HTMLTableRowElement> & Partial<TableRowProps>;
  }

  export interface TableColumnAttrExt {
    headerProps?: HTMLAttributes<HTMLTableHeaderCellElement> & Partial<TableHeaderProps>;
    cellProps?: HTMLAttributes<HTMLTableDataCellElement> & Partial<TableCellProps>;
    footerProps?: HTMLAttributes<HTMLTableDataCellElement> & Partial<TableFooterProps>;
  }

  export interface ColumnInstance<D extends object = {}>
    extends Omit<ColumnInterface<D>, "id">,
      ColumnInterfaceBasedOnValue<D>,
      UseTableColumnProps<D>,
      TableColumnAttr {}

  export interface UseTableProps<D extends object> {
    columns: ReadonlyArray<Column<D>>;
    data: readonly D[];
    tableAttr?: readonly TableAttr;
  }
}
