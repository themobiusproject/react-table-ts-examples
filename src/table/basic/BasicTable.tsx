import React from "react";
import { useTable, Column, TableAttr, UseTableProps } from "react-table";
import styles from "./BasicTable.module.css";

import makeData from "./makeData";

export interface Person {
  firstName: string;
  lastName: string;
  age: number;
  visits: number;
  status: "relationship" | "single" | "complicated";
  progress: number;
  subRows?: Person;
}

const Table = <D extends object>({ columns, data, tableAttr = {} }: UseTableProps<D>): JSX.Element => {
  // Use the state and functions returned from useTable to build your UI
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable({
    columns,
    data,
  });

  // Render the UI for your table
  return (
    <table {...getTableProps([{ ...tableAttr.tableProps }])}>
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps([{ ...tableAttr.headerGroupProps }])}>
            {headerGroup.headers.map((column) => (
              <th {...column.getHeaderProps([{ ...column.headerProps }])}>{column.render("Header")}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps([{ ...tableAttr.bodyProps }])}>
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps([{ ...tableAttr.rowProps }])}>
              {row.cells.map((cell) => {
                return <td {...cell.getCellProps([{ ...cell.column.cellProps }])}>{cell.render("Cell")}</td>;
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

const BasicTable = () => {
  const columns: Column<Person>[] = React.useMemo(
    () => [
      {
        Header: "Name",
        columns: [
          {
            Header: "First Name",
            accessor: "firstName",
            cellProps: { className: styles.td },
            headerProps: { className: styles.th },
          },
          {
            Header: "Last Name",
            accessor: "lastName",
            cellProps: { className: styles.td },
            headerProps: { className: styles.th },
          },
        ],
        headerProps: { className: styles.th },
      },
      {
        Header: "Info",
        columns: [
          {
            Header: "Age",
            accessor: "age",
            cellProps: { className: styles.td },
            headerProps: { className: styles.th },
          },
          {
            Header: "Visits",
            accessor: "visits",
            cellProps: { className: styles.td },
            headerProps: { className: styles.th },
          },
          {
            Header: "Status",
            accessor: "status",
            cellProps: { className: styles.td },
            headerProps: { className: styles.th },
          },
          {
            Header: "Profile Progress",
            accessor: "progress",
            cellProps: { className: styles.td },
            headerProps: { className: styles.th },
          },
        ],
        headerProps: { className: styles.th },
      },
    ],
    []
  );

  const tableAttr: TableAttr = React.useMemo(() => {
    return {
      tableProps: { className: styles.table },
      rowProps: { className: styles.tr },
    };
  }, []);

  const data = React.useMemo(() => makeData(20), []);

  return (
    <div className={styles.div}>
      <Table columns={columns} data={data} tableAttr={tableAttr} />
    </div>
  );
};

export default BasicTable;
